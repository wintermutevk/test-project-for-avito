package com.testproject.avito.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;

import com.testproject.avito.providers.UsersContentProvider;
import com.testproject.avito.api.RetrofitHelper;
import com.testproject.avito.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by user on 9/6/2015.
 */
public class WorkerService extends IntentService implements Callback<List<User>> {

    public final static String ACTION = "action";
    public final static String GET_USERS = "get_users";

    public WorkerService() {
        super("");

    }

    public WorkerService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getStringExtra(ACTION);
        if(GET_USERS.equals(action)) {
            RetrofitHelper.getApiGithub().getUsers(this);
        }
    }

    @Override
    public void success(List<User> userList, Response response) {
        ContentValues[] contentValues = new ContentValues[userList.size()];
        for(int i=0; i<userList.size(); i++) {
             contentValues[i] = userList.get(i).toContentValues();
        }
        getContentResolver().bulkInsert(UsersContentProvider.URI_USERS, contentValues);
    }

    @Override
    public void failure(RetrofitError error) {

    }
}
