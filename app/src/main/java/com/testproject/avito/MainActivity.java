package com.testproject.avito;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.testproject.avito.fragments.UsersFragment;
import com.testproject.avito.providers.UsersContentProvider;


public class MainActivity extends FragmentActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getContentResolver().delete(UsersContentProvider.URI_USERS, null, null);
        getSupportFragmentManager().beginTransaction().add(R.id.llMain, new UsersFragment()).commit();
    }

}
