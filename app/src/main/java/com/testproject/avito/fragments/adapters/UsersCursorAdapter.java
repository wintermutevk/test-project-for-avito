package com.testproject.avito.fragments.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.testproject.avito.R;
import com.testproject.avito.model.User;

/**
 * Created by user on 9/6/2015.
 */
public class UsersCursorAdapter extends CursorAdapter {

    public UsersCursorAdapter(Context context) {
        super(context, null, false);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_item, viewGroup, false);
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        User user = new User(cursor);
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        if(viewHolder == null) {
            viewHolder = new ViewHolder();
            viewHolder.ivAvatar = (ImageView) view.findViewById(R.id.ivAvatar);
            viewHolder.tvLogin = (TextView) view.findViewById(R.id.tvLogin);
            view.setTag(viewHolder);
        }
        viewHolder.tvLogin.setText(user.login);
        Picasso.with(context).load(user.avatar_url).resize(64, 64).centerCrop().into(viewHolder.ivAvatar);

    }

    private static class ViewHolder {
        public ImageView ivAvatar;
        public TextView tvLogin;
    }
}
