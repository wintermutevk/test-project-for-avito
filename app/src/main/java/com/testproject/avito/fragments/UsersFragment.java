package com.testproject.avito.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.testproject.avito.R;
import com.testproject.avito.api.RequestHelper;
import com.testproject.avito.fragments.adapters.UsersCursorAdapter;
import com.testproject.avito.providers.UsersContentProvider;

/**
 * Created by user on 9/6/2015.
 */
public class UsersFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private UsersCursorAdapter adapter;
    private LoaderManager loaderManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loaderManager = getActivity().getSupportLoaderManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_users, container, false);
        ListView listView = (ListView) v.findViewById(R.id.lvUsers);
        adapter = new UsersCursorAdapter(getActivity());
        listView.setAdapter(adapter);
        loaderManager.initLoader(0, null, this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RequestHelper.getInstance(getActivity()).getUsers();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), UsersContentProvider.URI_USERS, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        adapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}


