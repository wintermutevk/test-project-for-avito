package com.testproject.avito.api;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import com.testproject.avito.providers.UsersContentProvider;
import com.testproject.avito.services.WorkerService;

/**
 * Created by user on 9/6/2015.
 */
public class RequestHelper {

    private static RequestHelper requestHelper;

    public static RequestHelper getInstance(Context context) {
        if(requestHelper == null) {
            requestHelper = new RequestHelper(context);
        }
        return requestHelper;
    }

    private Context context;

    private RequestHelper(Context context) {
        this.context = context;
    }

    public void getUsers() {
        if(isEmpty(UsersContentProvider.URI_USERS)) {
            Intent intent = new Intent(context, WorkerService.class);
            intent.putExtra(WorkerService.ACTION, WorkerService.GET_USERS);
            context.startService(intent);
        }
    }

    private boolean isEmpty(Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if(cursor.moveToFirst()) {
            return false;
        }
        return true;
    }
}
