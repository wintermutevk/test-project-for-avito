package com.testproject.avito.api;

import android.util.Log;

import com.testproject.avito.api.ApiGithub;

import retrofit.RestAdapter;

/**
 * Created by user on 9/6/2015.
 */
public class RetrofitHelper {

    private static final String URL = "https://api.github.com";

    private static ApiGithub apiGithub;

    public static ApiGithub getApiGithub() {
        if(apiGithub == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setLog(new RestAdapter.Log() {
                        @Override
                        public void log(String message) {
                            Log.d("api_log", message);
                        }
                    })
                    .build();
            apiGithub = restAdapter.create(ApiGithub.class);
        }
        return  apiGithub;
    }
}
