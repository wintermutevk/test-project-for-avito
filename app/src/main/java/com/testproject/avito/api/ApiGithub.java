package com.testproject.avito.api;

import com.testproject.avito.model.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by user on 9/6/2015.
 */
public interface ApiGithub {

    @GET("/users")
    public void getUsers(Callback<List<User>> users);
}
