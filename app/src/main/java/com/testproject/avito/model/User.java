package com.testproject.avito.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.testproject.avito.providers.UsersContentProvider;

/**
 * Created by user on 9/6/2015.
 */
public class User {
    public String avatar_url;
    public String login;

    public User(Cursor cursor) {
        avatar_url = cursor.getString(cursor.getColumnIndex(UsersContentProvider.AVATAR));
        login = cursor.getString(cursor.getColumnIndex(UsersContentProvider.LOGIN));
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(UsersContentProvider.LOGIN, login);
        cv.put(UsersContentProvider.AVATAR, avatar_url);
        return cv;
    }
}
